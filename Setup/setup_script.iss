; Script generated by the Inno Script Studio Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!


#define MyAppName "Odebrecht BlocoK"
[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{5A115926-5F46-47BE-AE80-21667BE30A9A}
AppName={#MyAppName}
AppVersion=0.1
;AppVerName=Odebrecht - BlocoK 0.1
AppPublisher=Tecnologia Integrada
AppPublisherURL=http://www.tecnologiaintegrada.ca/
AppSupportURL=http://www.tecnologiaintegrada.ca/
AppUpdatesURL=http://www.tecnologiaintegrada.ca/
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName=Odebrecht - BlocoK
AllowNoIcons=yes
OutputDir=D:\Sistemas\odebrecht_blocok_sped\Setup
OutputBaseFilename=setup
Compression=lzma
SolidCompression=yes

[Languages]
Name: "brazilianportuguese"; MessagesFile: "compiler:Languages\BrazilianPortuguese.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\BlocoK.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\3DW.Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\EntityFramework.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\EntityFramework.SqlServer.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\ExcelDataReader.DataSet.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\ExcelDataReader.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\Newtonsoft.Json.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\System.Data.SQLite.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\System.Data.SQLite.EF6.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\System.Data.SQLite.Linq.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\System.Data.SQLite.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\System.Windows.Forms.Ribbon35.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\Data\*"; DestDir: "{app}\Data"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\Logs\*"; DestDir: "{app}\Logs"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\Reports\*"; DestDir: "{app}\Reports"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\x64\*"; DestDir: "{app}\x64"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\Sistemas\odebrecht_blocok_sped\Odebrecht_BlocoK\bin\Debug\x86\*"; DestDir: "{app}\x86"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\Odebrecht - BlocoK"; Filename: "{app}\BlocoK.exe"
Name: "{group}\{cm:ProgramOnTheWeb,Odebrecht - BlocoK}"; Filename: "http://www.tecnologiaintegrada.ca/"
Name: "{group}\{cm:UninstallProgram,Odebrecht - BlocoK}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\Odebrecht - BlocoK"; Filename: "{app}\BlocoK.exe"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Odebrecht - BlocoK"; Filename: "{app}\BlocoK.exe"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\BlocoK.exe"; Description: "{cm:LaunchProgram,Odebrecht - BlocoK}"; Flags: nowait postinstall skipifsilent
