﻿using Odebrecht_BlocoK.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.Adapters
{
    public class SQLiteAdapter
    {
        private SQLiteConnection Connection { get; set; }
        public String Path { get; set; }

        public SQLiteAdapter(String Path)
        {
            Connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;", Path));
            this.Path = Path;
        }

        public SQLiteAdapter() { }

        public DataTable GetData(String SQLCommand)
        {
            DataTable dataTable = new DataTable();
            using (var comm = new System.Data.SQLite.SQLiteCommand(this.Connection))
            {
                comm.CommandText = SQLCommand;
                var adapter = new SQLiteDataAdapter(comm);
                adapter.Fill(dataTable);
            }
            return dataTable;
        }

        public static String GetData(String FileName, String SQLCommand)
        {
            SQLiteAdapter myAdapter = new SQLiteAdapter(FileName);
            SQLiteDataAdapter dataAdapter = null;

            DataTable dataTable = new DataTable();
            
            using (var conn = myAdapter.Connection)
            {
                conn.Open();
                using (var comm = new System.Data.SQLite.SQLiteCommand(myAdapter.Connection))
                {
                    comm.CommandText = SQLCommand;
                    dataAdapter = new SQLiteDataAdapter(comm.CommandText,conn);
                    dataAdapter.Fill(dataTable);
                }
                conn.Close();
            }

            AdapterResponse response = new AdapterResponse(dataTable != null, dataTable);
            return response.ToJson();
        }

        public static String BulkInsert(DataTable source, String FileName, ProgressBar progressBar = null, String Mes = null, String Ano = null, String Mapa = null, String Carimbo = null)
        {
            Boolean Success = false;

            try
            {
                SQLiteAdapter adapter = new SQLiteAdapter(FileName);
                InsertData(adapter, source, progressBar, Mes, Ano, Mapa, Carimbo);
                Success = true;
            }catch(Exception ex)
            {
                Log.Make(ex, false);
            }

            return new AdapterResponse(Success).ToJson();
        }

        public static String Insert(String FileName, String SQL)
        {
            Boolean Success = true;

            SQLiteAdapter adapter = new SQLiteAdapter(FileName);

            using (var conn = adapter.Connection)
            {
                conn.Open();
                SQLiteCommand sql_cmd = conn.CreateCommand();

                sql_cmd.CommandText = SQL;

                try
                {
                    sql_cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Log.Make(ex, false);
                    Success = false;
                }
            }

            return new AdapterResponse(Success).ToJson();
        }

        public static void InsertData(SQLiteAdapter adapter, DataTable data, ProgressBar progressBar = null, String ParamMes = null, String ParamAno = null, String ParamMapa = null, String Carimbo = null)
        {
            String Ano = ParamAno == null ? DateTime.Now.Year.ToString() : ParamAno;
            String Mes = ParamMes == null ? DateTime.Now.Month.ToString() : ParamMes;


            if (progressBar != null)
            {
                progressBar.Maximum = data.Rows.Count;
                progressBar.Value = 0;
            }

            using (var conn = adapter.Connection)
            {
                conn.Open();
                SQLiteCommand sql_cmd = conn.CreateCommand();

                // 100,000 inserts
                for (var line = 1; line < data.Rows.Count; line++)
                {
                    String top = "INSERT INTO imports (";
                    String bottom = ") VALUES (";

                    top += String.Format("id_mapa,");
                    bottom += String.Format("{0},", ParamMapa);

                    top += String.Format("carimbo,");
                    bottom += String.Format("'{0}',", Carimbo);

                    for (var column = 1; column <= data.Columns.Count; column++)
                    {
                        top += String.Format("CAMPO{0},", column);
                        bottom += String.Format("'{0}',", data.Rows[line][column - 1]);
                    }

                    top += String.Format("mes,");
                    bottom += String.Format("{0},", Mes);

                    top += String.Format("ano,");
                    bottom += String.Format("{0},", Ano);

                    top += String.Format("criado_em,");
                    bottom += String.Format("'{0}',", DateTime.Now.ToShortDateString());

                    top += String.Format("criado_por,");
                    bottom += String.Format("'{0}',", Environment.UserName);

                    String SQL = String.Concat(top.Remove(top.Length - 1), bottom.Remove(bottom.Length - 1), ")");
                    sql_cmd.CommandText = SQL;

                    try
                    {
                        sql_cmd.ExecuteNonQuery();
                    }catch(Exception ex)
                    {
                        Log.Make(ex, false);
                    }

                    if (progressBar != null)
                    {
                        progressBar.Value = line;
                    }
                }

                conn.Close();

                if (progressBar != null)
                {
                    progressBar.Maximum = 100;
                    progressBar.Value = 0;
                }
            }
        }
    }
}
