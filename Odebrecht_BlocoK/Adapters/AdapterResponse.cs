﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.Adapters
{
    public class AdapterResponse
    {
        public Boolean Success { get; set; }
        public DataTable Data { get; set; }

        public AdapterResponse(Boolean Success = true, DataTable Data = null)
        {
            this.Success = Success;
            this.Data = Data;
        }

        public String ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AdapterResponse FromJSON(String Json)
        {
            return JsonConvert.DeserializeObject<AdapterResponse>(Json);
        }

        public void ShowDialod()
        {
            MessageBox.Show(
                    this.Success ? "Operação concluída" : "Erro ao processar operação",
                    this.Success ? "Sucesso" : "Erro",
                    MessageBoxButtons.OK, this.Success ? MessageBoxIcon.Information : MessageBoxIcon.Exclamation);
        }
    }
}
