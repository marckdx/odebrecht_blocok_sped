﻿using Odebrecht_BlocoK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Adapters
{
    public class Caller
    {        
        public static string Invoke(String typeName, String methodName, Object[] Parameters)
        {
            try
            {
                // Get the Type for the class
                Type calledType = Type.GetType(String.Format("Odebrecht_BlocoK.Adapters.{0}", typeName));

                // Invoke the method itself. The string returned by the method winds up in s.
                // Note that stringParam is passed via the last parameter of InvokeMember,
                // as an array of Objects.
                String s = (String)calledType.InvokeMember(methodName, BindingFlags.InvokeMethod | BindingFlags.Public |
                                    BindingFlags.Static, null, null, Parameters);

                // Return the string that was returned by the called method.
                return s;
            }catch(Exception ex)
            {
                Log.Make(ex, false);
            }

            return String.Empty;
        }

        internal static string Invoke(object driver, string v1, object[] v2)
        {
            throw new NotImplementedException();
        }
    }
}
