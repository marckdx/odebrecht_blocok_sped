﻿using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model
{
    public class K310
    {
        public String UO { get; set; }
        public String CONTA { get; set; }
        public Double VALOR
        {
            get
            {
               return CommonUtils.ToDouble(VALOR_STR);
            }
        }

        public String VALOR_STR { get; set; }
        public String NUMERO { get; set; }
        public String CONTABIL { get; set; }

        //public String CONTABIL
        //{
        //    get
        //    {
        //        return VALOR > 0 ? "D": "C";
        //    }
        //}
        public String ToPipe(String Separator)
        {
            return String.Concat(Separator,"K310", Separator, CommonUtils.MakeOnlyFour(UO) , Separator, VALOR.ToString().Replace('.', ','), Separator, CONTABIL, Separator);
        }
    }
}
