﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model
{
    public class JSONObject
    {
        public String ToJSON()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
