﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model
{
    public class Arquivo
    {
        public String Path { get { return _path; } }
        private String _path { get; set; }

        public String Content { get { return _content; } }
        private String _content { get; set; }

        public Arquivo(String Path, String Content)
        {
            this._path = Path;
            this._content = Content;
        }

        public static String ReadAll(String Path)
        {
            String TempContent = String.Empty;
            try
            {  
                using (StreamReader sr = new StreamReader(Path))
                {
                    TempContent = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Log.Make(ex);
            }

            return TempContent;
        }

        public static Arquivo ReadArquivo(String Path)
        {
            return new Arquivo(Path, ReadAll(Path));
        }

        public Boolean Save()
        {
            try
            {
                System.IO.File.WriteAllText(this.Path, this.Content);
            }catch(Exception ex)
            {
                Log.Make(ex);
                return false;
            }

            return true;
        }
    }
}
