﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.Model
{
    public class Log
    {
        public static void Make(Exception ex, Boolean ShowMessage = true)
        {
            if (ShowMessage)
            {
                MessageBox.Show(String.Format("Erro: {0} \nConsulte log para mais informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
