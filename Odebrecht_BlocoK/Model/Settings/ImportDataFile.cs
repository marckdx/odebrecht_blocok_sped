﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model.Settings
{
    public class ImportDataFile
    {
        public string Extensions { get; set; }
        public string Name { get; set; }
        public bool IsFile { get; set; }
        public bool IsWeb { get; set; }
    }
}
