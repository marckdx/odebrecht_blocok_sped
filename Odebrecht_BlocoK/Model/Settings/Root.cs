﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model.Settings
{
    public class Root:JSONObject
    {
        public List<ImportDataFile> ImportDataFiles { get; set; }
        public List<DataSource> DataSources { get; set; }
        public Boolean SkipSelectDataSourceWindow { get; set; }
        public Int32 DefaultDataSourceIndex { get; set; }

        public static Root FromFile(String Path)
        {
           return JsonConvert.DeserializeObject<Root>(Arquivo.ReadAll(Path));
        }
    }
}
