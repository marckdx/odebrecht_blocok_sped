﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model.Settings
{
    public class DataSource
    {
        public string Name { get; set; }
        public string Driver { get; set; }
        public string FilePath { get; set; }
        public Boolean IsFile { get; set; }
        public Boolean Embeded { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public Int32 Port { get; set; }

    }
}
