﻿using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Model
{
    public class K315
    {
        public String UO { get; set; }
        public String CONTA { get; set; }

        public Double VALOR { get; set; }

        public String CONTABIL
        {
            get; set;
        }

        public String VALOR_STR { get; set; }

        public K315() { }

        public K315(String UO, String CONTA, Double VALOR)
        {
            this.UO = UO;
            this.VALOR = VALOR;
            this.CONTA = CONTA;
        }

        public String ToPipe(String Separator = "|")
        {
            return String.Concat(Separator,"K315", Separator , CommonUtils.MakeOnlyFour(UO), Separator, CONTA, Separator, VALOR_STR.ToString().Replace('.', ','), Separator, CONTABIL, Separator);
        }

        
    }
}
