﻿using Odebrecht_BlocoK.Adapters;
using Odebrecht_BlocoK.Model;
using Odebrecht_BlocoK.Model.Settings;
using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.View
{
    public partial class FormMapas : Form
    {
        public Root Settings { get; set; }

        public FormMapas(Root Settings)
        {
            InitializeComponent();
            this.Settings = Settings;
            cboMes.SelectedIndex = cboMes.FindStringExact(DateTime.Now.Month.ToString());
            cboAno.Items.Add(DateTime.Now.Year.ToString());
            cboAno.SelectedIndex = 0;
            this.Settings = Settings;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Model.Settings.DataSource ResourceMain = Settings.DataSources[Settings.DefaultDataSourceIndex];
            String SQL = String.Format("SELECT * FROM maps");

            AdapterResponse response = AdapterResponse.FromJSON(Caller.Invoke(ResourceMain.Driver, "GetData", new Object[] {
                    ResourceMain.Embeded ? CommonUtils.ToCurrentPath(ResourceMain.FilePath) :  ResourceMain.FilePath,
                    SQL
                }));

            if (response.Success)
            {
                dtgDados.DataSource = response.Data;
            }
            else
            {
                response.ShowDialod();
            }

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            FormNewMapa mapa = new FormNewMapa(this.Settings);
            mapa.ShowDialog();
        }
    }
}
