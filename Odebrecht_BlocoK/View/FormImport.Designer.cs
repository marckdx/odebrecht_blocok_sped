﻿namespace Odebrecht_BlocoK.View
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormImport));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkGerarMapaAutomaticamente = new System.Windows.Forms.CheckBox();
            this.btnNewMapa = new System.Windows.Forms.Button();
            this.btnReload = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cboAno = new System.Windows.Forms.ComboBox();
            this.cboMes = new System.Windows.Forms.ComboBox();
            this.cboTables = new System.Windows.Forms.ComboBox();
            this.cboMapa = new System.Windows.Forms.ComboBox();
            this.cboDataSource = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.prbLoading = new System.Windows.Forms.ProgressBar();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnLoadDataSet = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dtgDados = new System.Windows.Forms.DataGridView();
            this.bindingSourceMain = new System.Windows.Forms.BindingSource(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.txtCarimbo = new System.Windows.Forms.TextBox();
            this.btnGerarBloco = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceMain)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 77);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Selecione uma origem de dados a serem importados";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Importação manual de dados";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Fonte:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtCarimbo);
            this.panel2.Controls.Add(this.chkGerarMapaAutomaticamente);
            this.panel2.Controls.Add(this.btnNewMapa);
            this.panel2.Controls.Add(this.btnReload);
            this.panel2.Controls.Add(this.btnLoad);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.cboAno);
            this.panel2.Controls.Add(this.cboMes);
            this.panel2.Controls.Add(this.cboTables);
            this.panel2.Controls.Add(this.cboMapa);
            this.panel2.Controls.Add(this.cboDataSource);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 77);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 117);
            this.panel2.TabIndex = 2;
            // 
            // chkGerarMapaAutomaticamente
            // 
            this.chkGerarMapaAutomaticamente.AutoSize = true;
            this.chkGerarMapaAutomaticamente.Location = new System.Drawing.Point(66, 94);
            this.chkGerarMapaAutomaticamente.Name = "chkGerarMapaAutomaticamente";
            this.chkGerarMapaAutomaticamente.Size = new System.Drawing.Size(128, 17);
            this.chkGerarMapaAutomaticamente.TabIndex = 6;
            this.chkGerarMapaAutomaticamente.Text = "Ignorar Primeira Linha";
            this.chkGerarMapaAutomaticamente.UseVisualStyleBackColor = true;
            this.chkGerarMapaAutomaticamente.CheckedChanged += new System.EventHandler(this.chkGerarMapaAutomaticamente_CheckedChanged);
            // 
            // btnNewMapa
            // 
            this.btnNewMapa.Location = new System.Drawing.Point(400, 66);
            this.btnNewMapa.Name = "btnNewMapa";
            this.btnNewMapa.Size = new System.Drawing.Size(31, 23);
            this.btnNewMapa.TabIndex = 5;
            this.btnNewMapa.Text = "+";
            this.btnNewMapa.UseVisualStyleBackColor = true;
            this.btnNewMapa.Click += new System.EventHandler(this.btnNewMapa_Click);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(433, 66);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(31, 23);
            this.btnReload.TabIndex = 5;
            this.btnReload.Text = "r";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(433, 39);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(31, 23);
            this.btnLoad.TabIndex = 5;
            this.btnLoad.Text = "...";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(66, 41);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(361, 20);
            this.textBox1.TabIndex = 4;
            // 
            // cboAno
            // 
            this.cboAno.FormattingEnabled = true;
            this.cboAno.Location = new System.Drawing.Point(643, 15);
            this.cboAno.Name = "cboAno";
            this.cboAno.Size = new System.Drawing.Size(109, 21);
            this.cboAno.TabIndex = 3;
            // 
            // cboMes
            // 
            this.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMes.FormattingEnabled = true;
            this.cboMes.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cboMes.Location = new System.Drawing.Point(553, 15);
            this.cboMes.Name = "cboMes";
            this.cboMes.Size = new System.Drawing.Size(84, 21);
            this.cboMes.TabIndex = 3;
            // 
            // cboTables
            // 
            this.cboTables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTables.FormattingEnabled = true;
            this.cboTables.Location = new System.Drawing.Point(553, 41);
            this.cboTables.Name = "cboTables";
            this.cboTables.Size = new System.Drawing.Size(199, 21);
            this.cboTables.TabIndex = 2;
            this.cboTables.SelectedIndexChanged += new System.EventHandler(this.cboTables_SelectedIndexChanged);
            // 
            // cboMapa
            // 
            this.cboMapa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMapa.FormattingEnabled = true;
            this.cboMapa.Location = new System.Drawing.Point(66, 68);
            this.cboMapa.Name = "cboMapa";
            this.cboMapa.Size = new System.Drawing.Size(328, 21);
            this.cboMapa.TabIndex = 2;
            // 
            // cboDataSource
            // 
            this.cboDataSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDataSource.FormattingEnabled = true;
            this.cboDataSource.Location = new System.Drawing.Point(66, 15);
            this.cboDataSource.Name = "cboDataSource";
            this.cboDataSource.Size = new System.Drawing.Size(398, 21);
            this.cboDataSource.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(475, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Competência:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(480, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Aba/Tabela:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Origem:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Mapa:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 509);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(784, 52);
            this.panel3.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.prbLoading);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(323, 52);
            this.panel6.TabIndex = 3;
            // 
            // prbLoading
            // 
            this.prbLoading.Location = new System.Drawing.Point(12, 10);
            this.prbLoading.Name = "prbLoading";
            this.prbLoading.Size = new System.Drawing.Size(230, 23);
            this.prbLoading.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.button3);
            this.panel5.Controls.Add(this.btnLoadDataSet);
            this.panel5.Controls.Add(this.btnGerarBloco);
            this.panel5.Controls.Add(this.btnImport);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(323, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(461, 52);
            this.panel5.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(30, 10);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(104, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "Cancelar (Esc)";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnLoadDataSet
            // 
            this.btnLoadDataSet.Location = new System.Drawing.Point(140, 10);
            this.btnLoadDataSet.Name = "btnLoadDataSet";
            this.btnLoadDataSet.Size = new System.Drawing.Size(94, 23);
            this.btnLoadDataSet.TabIndex = 0;
            this.btnLoadDataSet.Text = "Carregar (F5)";
            this.btnLoadDataSet.UseVisualStyleBackColor = true;
            this.btnLoadDataSet.Click += new System.EventHandler(this.btnLoadDataSet_Click);
            // 
            // btnImport
            // 
            this.btnImport.Enabled = false;
            this.btnImport.Location = new System.Drawing.Point(239, 10);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(101, 23);
            this.btnImport.TabIndex = 0;
            this.btnImport.Text = "Importar (Enter)";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dtgDados);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 194);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(784, 315);
            this.panel4.TabIndex = 4;
            // 
            // dtgDados
            // 
            this.dtgDados.BackgroundColor = System.Drawing.Color.White;
            this.dtgDados.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dtgDados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgDados.Location = new System.Drawing.Point(0, 0);
            this.dtgDados.Name = "dtgDados";
            this.dtgDados.Size = new System.Drawing.Size(784, 315);
            this.dtgDados.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(499, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Carimbo:";
            // 
            // txtCarimbo
            // 
            this.txtCarimbo.Location = new System.Drawing.Point(553, 68);
            this.txtCarimbo.Name = "txtCarimbo";
            this.txtCarimbo.ReadOnly = true;
            this.txtCarimbo.Size = new System.Drawing.Size(199, 20);
            this.txtCarimbo.TabIndex = 7;
            // 
            // btnGerarBloco
            // 
            this.btnGerarBloco.Enabled = false;
            this.btnGerarBloco.Location = new System.Drawing.Point(346, 10);
            this.btnGerarBloco.Name = "btnGerarBloco";
            this.btnGerarBloco.Size = new System.Drawing.Size(101, 23);
            this.btnGerarBloco.TabIndex = 0;
            this.btnGerarBloco.Text = "Gerar Bloco";
            this.btnGerarBloco.UseVisualStyleBackColor = true;
            this.btnGerarBloco.Click += new System.EventHandler(this.btnGerarBloco_Click);
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "3DW Lite - Importar Dados";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgDados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cboAno;
        private System.Windows.Forms.ComboBox cboMes;
        private System.Windows.Forms.ComboBox cboTables;
        private System.Windows.Forms.ComboBox cboDataSource;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dtgDados;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnLoadDataSet;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.ComboBox cboMapa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkGerarMapaAutomaticamente;
        private System.Windows.Forms.BindingSource bindingSourceMain;
        private System.Windows.Forms.ProgressBar prbLoading;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.Button btnNewMapa;
        private System.Windows.Forms.TextBox txtCarimbo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnGerarBloco;
    }
}