﻿using Odebrecht_BlocoK.Model.Settings;
using Odebrecht_BlocoK.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK
{
    public partial class FormMain : Form
    {
        private Root Settings { get; set; }

        public FormMain(Root Settings)
        {
            InitializeComponent();
            this.Settings = Settings;
        }

        private void btnImportData_Click(object sender, EventArgs e)
        {
            FormImport import = new FormImport(Settings);
            import.Show();
        }

        private void rbTickets_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.ebuzzy.com.br/login");
        }

        private void rbSite_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.tecnologiaintegrada.ca");
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            FormHistorico historico = new FormHistorico(this.Settings);
            historico.Show();
        }

        private void btnMapas_Click(object sender, EventArgs e)
        {
            FormMapas mapas = new FormMapas(this.Settings);
            mapas.Show();
        }

        private void btnGerarBlocoK_Click(object sender, EventArgs e)
        {
            FormBlocoK blocoK = new FormBlocoK(this.Settings);
            blocoK.ShowDialog();
        }
    }
}
