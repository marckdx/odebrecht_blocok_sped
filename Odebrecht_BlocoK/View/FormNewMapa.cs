﻿using Odebrecht_BlocoK.Adapters;
using Odebrecht_BlocoK.Model;
using Odebrecht_BlocoK.Model.Settings;
using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.View
{
    public partial class FormNewMapa : Form
    {
        private Root Settings { get; set; }
        private DataTable dtDataTypes { get; set; }

        public FormNewMapa(Root Settings)
        {
            InitializeComponent();
            this.Settings = Settings;

            LoadData();
        }

        private void LoadData()
        {
            Model.Settings.DataSource ResourceMain = Settings.DataSources[Settings.DefaultDataSourceIndex];
            String SQL = String.Format("SELECT * FROM data_types");

            AdapterResponse response = AdapterResponse.FromJSON(Caller.Invoke(ResourceMain.Driver, "GetData", new Object[] {
                    ResourceMain.Embeded ? CommonUtils.ToCurrentPath(ResourceMain.FilePath) :  ResourceMain.FilePath,
                    SQL
                }));

            this.dtDataTypes = response.Data.Copy();

            for (int i = 1; i <= 15; i++)
            {
                ComboBox comboBox = (ComboBox) GetComboBox(String.Format("cboTipoDadoCampo{0}", i));
                comboBox.DataSource = response.Data.Copy();
                comboBox.DisplayMember = "name";
                comboBox.ValueMember = "id";


            }
        }

        public TextBox GetTextBox(String name)
        {
            return ((TextBox)this.Controls.Find(name, true)[0]);
        }

        public ComboBox GetComboBox(String name)
        {
            return ((ComboBox)this.Controls.Find(name, true)[0]);
        }

        private void btnInsertMap_Click(object sender, EventArgs e)
        {
            String Top = "INSERT INTO maps (";
            String Bottom = ") VALUES (";

            Top += "name,";
            Bottom += String.Format("'{0}',", txtNomeMapa.Text);

            for(int i=1; i <= 15; i++)
            {
                Top += String.Format("rotulo_campo{0},", i);
                Bottom += String.Format("'{0}',", GetTextBox(String.Format("txtRotuloCampo{0}", i)).Text.ToString());

                Top += String.Format("nome_campo{0},", i);
                Bottom += String.Format("'{0}',", GetTextBox(String.Format("txtCampo{0}", i)).Text.ToString());

                Top += String.Format("tipo_dado_campo{0},", i);

                String IdTipoDado = this.dtDataTypes.Rows[GetComboBox(String.Format("cboTipoDadoCampo{0}", i)).SelectedIndex]["id"].ToString();
                Bottom += String.Format("{0},", IdTipoDado);
            }

            String SQL = String.Concat(Top.Remove(Top.Length -1 ), Bottom.Remove(Bottom.Length -1) ,")");

            Model.Settings.DataSource ResourceMain = Settings.DataSources[Settings.DefaultDataSourceIndex];

            AdapterResponse response = AdapterResponse.FromJSON(Caller.Invoke(ResourceMain.Driver, "Insert", new Object[] {
                    ResourceMain.Embeded ? CommonUtils.ToCurrentPath(ResourceMain.FilePath) :  ResourceMain.FilePath,
                    SQL
                }));

            response.ShowDialod();
        }
    }
}
