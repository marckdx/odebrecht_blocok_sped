﻿using Odebrecht_BlocoK.Adapters;
using Odebrecht_BlocoK.Model;
using Odebrecht_BlocoK.Model.Settings;
using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.View
{
    public partial class FormBlocoK : Form
    {
        private string Mes;
        private string Ano;
        private string Carimbo;
        private FormImport formImport;
        
        private Root Settings { get; set; }
        private StringBuilder Builder { get; set; }

        private Int32 MapaK100 { get { return 1; } }
        private Int32 MapaK110eK115 { get { return 2; } }
        private Int32 MapaK200eK300 { get { return 3; } }
        private Int32 MapaK210eK300 { get { return 4; } }
        private Int32 MapaK310eK315 { get { return 5; } }

        private DataTable DataCarimbo { get; set; }

        private String CarimboCombobox
        {
            get
            {
                return cboCarimbo.Text;
            }
        }

        public FormBlocoK(Root settings, string Mes, string Ano, string Carimbo, FormImport formImport)
        {
            InitializeComponent();

            this.Mes = Mes;
            this.Ano = Ano;
            this.Carimbo = Carimbo;
            this.formImport = formImport;

            /* CARREGA */
            
            this.Settings = Settings;
            this.Builder = new StringBuilder();

            GetCarimbos();

            cboMes.SelectedIndex = cboMes.FindStringExact(DateTime.Now.Month.ToString());
            cboAno.Items.Add(DateTime.Now.Year.ToString());
            cboAno.SelectedIndex = 0;

            LoadFastGenerate();
        }

        private void LoadFastGenerate()
        {
            try
            {
                cboMes.SelectedIndex = cboMes.FindStringExact(this.Mes);
                cboAno.SelectedIndex = cboMes.FindStringExact(this.Ano);
                cboCarimbo.SelectedIndex = cboMes.FindStringExact(this.Carimbo);
            }catch(Exception ex)
            {

            }
        }

        public FormBlocoK(Root Settings)
        {
            InitializeComponent();
            this.Settings = Settings;
            this.Builder = new StringBuilder();

            GetCarimbos();

            cboMes.SelectedIndex = cboMes.FindStringExact(DateTime.Now.Month.ToString());
            cboAno.Items.Add(DateTime.Now.Year.ToString());
            cboAno.SelectedIndex = 0;
        }

        private void GetCarimbos()
        {
            String SQL = "SELECT DISTINCT carimbo, mes, ano FROM imports";
            this.DataCarimbo = GetDataTable(SQL);
            cboCarimbo.DataSource = DataCarimbo;
            cboCarimbo.ValueMember = "carimbo";
            cboCarimbo.DisplayMember = "carimbo";
        }

        public void ChangeCarimbos()
        {
            var results = from DataRow Row in this.DataCarimbo.Rows
                          where 
                            Row[1].ToString().Equals(cboMes.Text.ToString()) &&
                            Row[2].ToString().Equals(cboAno.Text.ToString())
                          select Row;

            if (results.Count() > 0)
            {
                this.DataCarimbo = results.CopyToDataTable();
                cboCarimbo.DataSource = DataCarimbo;
                cboCarimbo.ValueMember = "carimbo";
                cboCarimbo.DisplayMember = "carimbo";
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                GerarBlocoK();
            }
            catch (Exception ex)
            {
                Log.Make(ex, true);
            }
        }

        private void GerarBlocoK()
        {
            Builder = new StringBuilder();
            txtBlocoK.Text = String.Empty;

            String[] ListK100 = GetK100();

            DataTable DataK110 = GetDataK110();
            String[] ListK110 = DataTableToArray(DataK110);

            DataTable DataK115 = GetDataK115();
            String[] ListK115 = DataTableToArray(DataK115);

            Builder.AppendLine("|K001|0|");

            GetDateFile();

            //Gerar linhas do K100
            foreach (String LineK100 in ListK100)
            {
                Builder.AppendLine(TreatLine(LineK100));
            }

            Int32 CountLineK110 = 0;
            foreach (String LineK110 in ListK110)
            {
                String COD_EVENTO = DataK110.Rows[CountLineK110]["campo4"].ToString();
                String DT_EVENTO = DataK110.Rows[CountLineK110]["campo5"].ToString();

                if (!COD_EVENTO.Equals(string.Empty) && !DT_EVENTO.Equals(string.Empty))
                {
                    Builder.AppendLine(TreatLine(LineK110));

                    var results = from DataRow Row in DataK115.Rows
                                  where Row["campo4"].Equals(COD_EVENTO) && Row["campo5"].Equals(DT_EVENTO)
                                  select Row;

                    foreach (DataRow Row in results)
                    {
                        Builder.AppendLine(DataRowK115ToString(Row, DataK115.Columns.Count, "|"));
                    }
                }

                CountLineK110++;
            }

            //Gerar Linhas do K200
            DataTable DataK200 = GetDataK200();

            DataTable DataK200Complete = GetDataK200Complete();
            DataTable DataK210Complete = GetDataK210Complete();

            String[] ListK200 = DataTableToArray(DataK200);

            DataTable DataK210 = GetDataK210();
            String[] ListK210 = DataTableToArray(DataK210);

            Int32 CountLineK200 = 0;
            foreach (String LineK200 in ListK200)
            {
                Builder.AppendLine(TreatLine(LineK200));

                String UOOE = DataK200Complete.Rows[CountLineK200][5].ToString();

                // CAMPO2 => Índice 1
                var results = from DataRow Row in DataK210Complete.Rows
                              where Row[4].Equals(UOOE)
                              select Row;

                foreach (DataRow Row in results)
                {
                    Builder.AppendLine(DataRowK210ToString(Row, DataK210.Columns.Count, "|", "K210", new Int32[] { 2, 8 }));
                }

                CountLineK200++;
            }

            //Gerar Linhas do K300
            DataTable DataK300 = GetDataK300();
            String[] ListK300 = DataTableToArray(DataK300);


            Int32 CountLineK300 = 0;
            foreach (String LineK300 in ListK300)
            {
                Builder.AppendLine(LineK300.Replace("-", String.Empty));
                //BUSCA K310
                String ContaK300 = DataK300.Rows[CountLineK300]["campo5"].ToString();
                //String UOK300 = DataK300.Rows[CountLineK300]["campo1"].ToString();

                IEnumerable<K310> K310ArrayParent = GetK310FromConta(ContaK300).Distinct();
               
                foreach (K310 K310Object in K310ArrayParent)
                {
                    List<String> Numeros = GetNumerosK310(K310Object.CONTA, K310Object.UO);

                    Builder.AppendLine(K310Object.ToPipe("|"));
                    GetK315FromK310(K310Object, Numeros);
                    /*GetK315FromK310(K310Object, 
                        new String[] {
                            K310Object.NUMERO
                        }.ToList());*/
                }

                CountLineK300++;
            }

            Builder.AppendLine(String.Concat("|K990|", (GetLines() + 2), "|"));
            lblLinhas.Text = String.Format("Linhas: {0}", (GetLines() + 1));
            txtBlocoK.Text = Builder.ToString();
        }

        private Int32 GetLines()
        {
           return Regex.Matches(Builder.ToString(), Environment.NewLine).Count;
        }

        private void GetDateFile()
        {
            String SQL = String.Format("SELECT campo1, campo2 FROM imports i WHERE i.id_mapa=1 AND i.mes={0} AND i.ano={1} AND i.carimbo='{2}' limit 1", cboMes.Text, cboAno.Text, CarimboCombobox);
            DataTable table = GetDataTable(SQL);

            String DataInicio = String.Empty;
            String DataTermino = String.Empty;

            try
            {
                DataInicio = ToData(table.Rows[0]["campo1"].ToString());
                DataTermino = ToData(table.Rows[0]["campo2"].ToString());
            }catch(Exception ex)
            {
                Log.Make(ex, false);
            }

            Builder.AppendLine(String.Concat("|K030|",DataInicio, "|", DataTermino, "|"));
        }

        public String ToData(String Origem)
        {
            return
                Origem.Equals(String.Empty) ? String.Empty :
                String.Concat(Origem.Substring(0, 2), Origem.Substring(3, 2), Origem.Substring(6, 4));
        }

        private String GetK315FromK310(K310 K310Object, List<String> Numeros)
        {
            String SQL = String.Format(@"SELECT campo2, campo4, replace(sum(replace(campo6, ',', '.')), '-', '') campo6, campo7 FROM imports i WHERE i.id_mapa=5 and campo1 !='' 
                    AND campo4 != '{4}'  and mes={5} and ano={6} and carimbo='{7}' and campo1 in ({8}) group by campo2, campo4", K310Object.CONTA, cboMes.Text, cboAno.Text, CarimboCombobox,
                    K310Object.CONTA, cboMes.Text, cboAno.Text, CarimboCombobox, ListStringToString(Numeros));

            DataTable DataK315 = GetDataTable(SQL);

            IEnumerable<K315> K315ArrayParent = from DataRow RowK315Sum in DataK315.Rows
                                                select new K315
                                                {
                                                    UO = RowK315Sum["campo2"].ToString(),
                                                    CONTA = RowK315Sum["campo4"].ToString(),
                                                    VALOR_STR = RowK315Sum["campo6"].ToString(),
                                                    CONTABIL = RowK315Sum["campo7"].ToString(),
                                                };

            foreach (K315 K315Object in K315ArrayParent)
            {
                Builder.AppendLine(K315Object.ToPipe("|"));
            }

            return String.Empty;
        }

        private String ListStringToString(List<String> Numeros)
        {
            String Response = String.Empty;

            foreach (String Numero in Numeros)
            {
                Response += String.Format("'{0}',", Numero);
            }

            return Response.Remove(Response.Length - 1);
        }

        private void GeraFilhosK15(List<String> Numeros, List<String> Contas, IEnumerable<K310> K310Array, DataTable DataK310)
        {
            Int32 CountNumeros = 0;
            foreach (String Numero in Numeros)
            {
                //Builder.AppendLine(DataRowToString(K310Array[CountNumeros], K310Array.Columns.Count, "|", "K310", new Int32[] {
                //      2,6,7
                //}));

                Builder.AppendLine(K310Array.ToList()[CountNumeros].ToPipe("|"));
                String Conta = Contas[CountNumeros];


                IEnumerable<K315> K315ArrayParent = from DataRow RowK315Sum in DataK310.Rows
                                                        where RowK315Sum[1].ToString().Equals(Numero) 
                                                            && RowK315Sum[4].ToString() != Conta
                                                        select new K315
                                                        {
                                                            UO = RowK315Sum[2].ToString(),
                                                            CONTA = RowK315Sum[4].ToString(),
                                                            VALOR_STR = RowK315Sum[6].ToString()
                                                        };

                foreach (K315 K315Object in K315ArrayParent)
                {
                    Builder.AppendLine(K315Object.ToPipe());
                }

                CountNumeros++;
            }
        }

        private List<String> GetNumerosK310(String CONTA, String UO)
        {
            String SQL = String.Format("select campo1 as Numero from imports where id_mapa=5 and campo6 != '' and campo4='{0}' AND mes={1} and ano={2} and carimbo='{3}' and campo2='{4}'", CONTA, cboMes.Text, cboAno.Text, CarimboCombobox, UO);
            DataTable table = GetDataTable(SQL);

            List<String> Numeros = table.AsEnumerable()
                           .Select(r => r.Field<string>("Numero"))
                           .ToList();

            return Numeros;
        }

        private IEnumerable<K310> GetK310FromConta(String CONTA)
        {
            String SQL = String.Format("select distinct campo1 as Numero, campo2 as UO, campo4 as Conta, sum(REPLACE(campo6, ',', '.')) as Valor, campo7 as DC from imports where id_mapa=5 and campo6 != '' and campo4='{0}' AND mes={1} and ano={2} and carimbo='{3}' group by campo2, campo4", CONTA, cboMes.Text, cboAno.Text, CarimboCombobox);
            DataTable table = GetDataTable(SQL);

            IEnumerable<K310> convertedList = (from Row in table.AsEnumerable()
                                 select new K310()
                                 {
                                     NUMERO = Row["Numero"].ToString(),
                                     UO = Row["UO"].ToString(),
                                     CONTA = Row["Conta"].ToString(),
                                     VALOR_STR = Row["Valor"].ToString().Replace("-", String.Empty),
                                     CONTABIL = Row["DC"].ToString()
                                 }).ToList();

            return convertedList; //.Select(o => o.CONTA).Distinct();
        }

        private DataTable GetDataK310()
        {
            String SQL = String.Format("select * from imports where id_mapa={0}  and campo6 != '' AND mes={1} AND ano={2} and carimbo='{3}'", MapaK310eK315, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private DataTable GetDataK200Complete()
        {
            String SQL = String.Format("select * from imports where id_mapa={0} and campo8 != '0' AND mes={1} AND ano={2} and carimbo='{3}'", MapaK200eK300, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private DataTable GetDataK210Complete()
        {
            String SQL = String.Format("SELECT * FROM imports WHERE id_mapa={0} AND mes={1} AND ano={2} and carimbo='{3}'", MapaK210eK300, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private DataTable GetDataK210()
        {
            String SQL = String.Format("SELECT 'K210', I210.campo2 as campo2, I210.CAMPO8 as campo8 FROM imports I200  JOIN imports I210 ON I210.campo4=I200.campo5 WHERE I200.id_mapa={0} AND I210.id_mapa={1} AND I210.mes={2} AND I210.ano={3} AND I210.carimbo='{4}' AND I200.mes={5} AND I200.ano={6} AND I200.carimbo='{7}'", MapaK210eK300, MapaK210eK300, cboMes.Text, cboAno.Text, CarimboCombobox, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private DataTable GetDataK300()
        {
            //and (campo4 = '4' OR campo4 = '3')
            String SQL = String.Format("select 'K300', campo5, campo8, campo9, campo10, campo11, campo12, campo13 from imports where id_mapa={0}  and campo8 != '0' AND mes={1} AND ano={2} AND campo3='A' and carimbo='{3}'", MapaK200eK300, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private DataTable GetDataK200()
        {
            String SQL = String.Format("select 'K200', '0' || campo2 as campo2, campo3, campo4, campo5, campo6, campo7 from imports where id_mapa={0} and campo8 != '0' AND mes={1} AND ano={2} AND carimbo='{3}'", MapaK200eK300, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private string DataRowK115ToString(DataRow Row, Int32 CountColumns, String Separator)
        {
            String TempRow = String.Empty;
            TempRow += String.Concat("|","K115", Separator);
            TempRow += String.Concat(Row["campo6"], Separator);
            TempRow += String.Concat(Row["campo8"], Separator);
            TempRow += String.Concat(Row["campo9"], Separator);
            return TempRow;
        }

        private string DataRowToString(DataRow Row, Int32 CountColumns, String Separator, String K= null, Int32[] Numbers = null)
        {
            String TempRow = K !=null ? String.Concat(K, Separator) :  String.Empty;

            if (Numbers == null)
            {
                for (int i = 0; i < CountColumns; i++)
                {
                    TempRow += String.Concat(Row[i], Separator);
                }
            }
            else
            {
                for (int i = 0; i < Numbers.Length; i++)
                {
                    TempRow += String.Concat(Row[Numbers[i]], Separator);
                }
            }

            return TempRow;
        }

        private string DataRowK210ToString(DataRow Row, Int32 CountColumns, String Separator, String K = null, Int32[] Numbers = null)
        {
            String TempRow = String.Empty;
            try
            {
                TempRow = K != null ? String.Concat(Separator, K, Separator) : Separator;

                if (Numbers == null)
                {
                    for (int i = 0; i < CountColumns; i++)
                    {
                        TempRow += String.Concat(Row[i], Separator);
                    }
                }
                else
                {
                    for (int i = 0; i < Numbers.Length; i++)
                    {
                        if (i == 0)
                        {
                            String content = Row[Numbers[i]].ToString();
                            content = CommonUtils.MakeOnlyFour(content);

                            TempRow += String.Concat(content, Separator);
                        }
                        else
                        {
                            TempRow += String.Concat(Row[Numbers[i]], Separator);
                        }
                    }
                }
            }catch(Exception Ex)
            {
                Log.Make(Ex, false);
            }

            return TempRow;
        }

        private string TreatLine(String LineK100)
        {
            String Temp = LineK100.Replace(":", String.Empty);
            Temp = Temp.Replace("NÃO", "N");
            Temp = Temp.Replace("não", "N");
            Temp = Temp.Replace("SIM", "S");
            Temp = Temp.Replace("sim", "S");
            Temp = Temp.Replace("/", String.Empty);
            //Temp = Temp.Replace(".", String.Empty);
            Temp = Temp.Replace("-", String.Empty);
            return Temp;
        }

        public DataTable GetDataTable(String SQL)
        {
            DataSource ResourceMain = Settings.DataSources[Settings.DefaultDataSourceIndex];

            AdapterResponse response = AdapterResponse.FromJSON(Caller.Invoke(ResourceMain.Driver, "GetData", new Object[] {
                    ResourceMain.Embeded ? CommonUtils.ToCurrentPath(ResourceMain.FilePath) :  ResourceMain.FilePath,
                    SQL
                }));

            return response.Data;
        }

        public String[] GetK100()
        {
            String SQL = String.Format("SELECT 'K100', '0' ||CAMPO4 as CAMPO4, substr(CAMPO5, 3, 4) AS CAMPO5, substr(REPLACE(CAMPO6, '.', ''), 0, 9),CAMPO7,CAMPO8,CAMPO10,CAMPO11,substr(CAMPO12,1,2) || substr(CAMPO12,4,2) ||  substr(CAMPO12,7,4) as CAMPO12, substr(CAMPO13,1,2) || substr(CAMPO13,4,2) ||  substr(CAMPO13,7,4) as CAMPO13 from imports WHERE id_mapa={0} AND mes={1} AND ano={2} and carimbo='{3}'", MapaK100, cboMes.Text, cboAno.Text, CarimboCombobox);
            DataTable DataK100 = GetDataTable(SQL);
            return DataTableToArray(DataK100, "|");
        }

        public String[] GetK110()
        {
            String SQL = String.Format("select distinct 'K110', campo4, campo5 from imports where id_mapa={0} and campo4 !='' AND mes={1} AND ano={2} AND carimbo='{3}'", MapaK100, cboMes.Text, cboAno.Text, CarimboCombobox);
            DataTable DataK100 = GetDataTable(SQL);
            return DataTableToArray(DataK100, "|");
        }

        public String[] GetK115()
        {
            String SQL = String.Format("select distinct 'K115', campo1,campo2,campo3,campo4,campo5,campo6,campo7,campo8,campo9 from imports where id_mapa={0} AND mes={1} AND ano={2} AND carimbo='{3}'", MapaK110eK115, cboMes.Text, cboAno.Text, CarimboCombobox);
            DataTable DataK100 = GetDataTable(SQL);
            return DataTableToArray(DataK100, "|");
        }

        public DataTable GetDataK115()
        {
            String SQL = String.Format("select distinct 'K115', campo1,campo2,campo3,campo4,campo5,campo6,campo7,campo8,campo9 from imports where id_mapa={0} AND mes={1} AND ano={2} AND carimbo='{3}'", MapaK110eK115, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        public DataTable GetDataK110()
        {
            String SQL = String.Format("select distinct 'K110', campo4, campo5 from imports where id_mapa={0} AND mes={1} AND ano={2} AND carimbo='{3}'", MapaK110eK115, cboMes.Text, cboAno.Text, CarimboCombobox);
            return GetDataTable(SQL);
        }

        private string[] DataTableToArray(DataTable Data, String Separator = "|")
        {
            List<String> TempList = new List<String>();

            foreach (DataRow Row in Data.Rows)
            {
                String TempRow = Separator;

                for(int i = 0; i < Data.Columns.Count; i++)
                {
                    TempRow += String.Concat(Row[i], Separator);
                }
                TempList.Add(TempRow);
            }

            return TempList.ToArray();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {  
            SaveFileDialog dlgOpen = new SaveFileDialog();
            dlgOpen.Filter = "Arquivo TXT|*.txt|Arquivo 3DW Lite|*.3dl";
            dlgOpen.Title = "Save an Image File";
            dlgOpen.InitialDirectory = CommonUtils.ToCurrentPath(@"\Reports");
            dlgOpen.FileName = String.Format("BlocoK_{0}_{1}_{2}", cboMes.Text, cboAno.Text, CarimboCombobox);

            dlgOpen.ShowDialog();
 
            if (dlgOpen.FileName != "")
            {
                Arquivo arquivo = new Arquivo(dlgOpen.FileName, txtBlocoK.Text);
                AdapterResponse response = new AdapterResponse(arquivo.Save());
                response.ShowDialod();
            }
             
        }

        private void cboMes_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeCarimbos();
        }

        private void cboAno_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeCarimbos();
        }
    }
}
