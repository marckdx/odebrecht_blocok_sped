﻿namespace Odebrecht_BlocoK
{
    partial class FormMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.rbDados = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.rbAjudaOnline = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.btnImportData = new System.Windows.Forms.RibbonButton();
            this.btnMapas = new System.Windows.Forms.RibbonButton();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.btnHistory = new System.Windows.Forms.RibbonButton();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton5 = new System.Windows.Forms.RibbonButton();
            this.btnGerarBlocoK = new System.Windows.Forms.RibbonButton();
            this.rbTickets = new System.Windows.Forms.RibbonButton();
            this.rbSite = new System.Windows.Forms.RibbonButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbon1
            // 
            this.ribbon1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(527, 447);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2013;
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.ribbon1.Size = new System.Drawing.Size(784, 147);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.rbDados);
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.Tabs.Add(this.ribbonTab2);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 26, 20, 0);
            this.ribbon1.Text = "ribbon1";
            this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            // 
            // rbDados
            // 
            this.rbDados.Panels.Add(this.ribbonPanel1);
            this.rbDados.Panels.Add(this.ribbonPanel4);
            this.rbDados.Panels.Add(this.ribbonPanel5);
            this.rbDados.Text = "Home";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.Items.Add(this.btnImportData);
            this.ribbonPanel1.Items.Add(this.btnMapas);
            this.ribbonPanel1.Items.Add(this.ribbonButton3);
            this.ribbonPanel1.Items.Add(this.btnHistory);
            this.ribbonPanel1.Text = "Dados";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.Items.Add(this.ribbonButton1);
            this.ribbonPanel4.Items.Add(this.ribbonButton4);
            this.ribbonPanel4.Items.Add(this.ribbonButton5);
            this.ribbonPanel4.Text = "Tratamento";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel2);
            this.ribbonTab1.Panels.Add(this.ribbonPanel3);
            this.ribbonTab1.Text = "Apresentação";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.Text = "WebReport";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.Text = "Relatórios";
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.rbAjudaOnline);
            this.ribbonTab2.Text = "Extra";
            // 
            // rbAjudaOnline
            // 
            this.rbAjudaOnline.Items.Add(this.rbTickets);
            this.rbAjudaOnline.Items.Add(this.rbSite);
            this.rbAjudaOnline.Text = "Ajuda";
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.Items.Add(this.btnGerarBlocoK);
            this.ribbonPanel5.Text = "Bloco K";
            // 
            // btnImportData
            // 
            this.btnImportData.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_csv_40;
            this.btnImportData.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnImportData.SmallImage")));
            this.btnImportData.Text = "Importar";
            this.btnImportData.Click += new System.EventHandler(this.btnImportData_Click);
            // 
            // btnMapas
            // 
            this.btnMapas.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_mapa_40;
            this.btnMapas.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnMapas.SmallImage")));
            this.btnMapas.Text = "Mapas";
            this.btnMapas.Click += new System.EventHandler(this.btnMapas_Click);
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Enabled = false;
            this.ribbonButton3.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_exportar_csv_40;
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Text = "Exportar";
            // 
            // btnHistory
            // 
            this.btnHistory.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_relógio_40;
            this.btnHistory.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnHistory.SmallImage")));
            this.btnHistory.Text = "Histórico";
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Enabled = false;
            this.ribbonButton1.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_metamorphose_40;
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "Visões";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.Enabled = false;
            this.ribbonButton4.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_filtro_preenchido_40;
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Text = "Regras";
            // 
            // ribbonButton5
            // 
            this.ribbonButton5.Enabled = false;
            this.ribbonButton5.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_consulta_de_junção_externa_40;
            this.ribbonButton5.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.SmallImage")));
            this.ribbonButton5.Text = "CrossApply";
            // 
            // btnGerarBlocoK
            // 
            this.btnGerarBlocoK.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_recibo_40;
            this.btnGerarBlocoK.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnGerarBlocoK.SmallImage")));
            this.btnGerarBlocoK.Text = "Gerar";
            this.btnGerarBlocoK.Click += new System.EventHandler(this.btnGerarBlocoK_Click);
            // 
            // rbTickets
            // 
            this.rbTickets.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_informações_40;
            this.rbTickets.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbTickets.SmallImage")));
            this.rbTickets.Text = "Chamados";
            this.rbTickets.Click += new System.EventHandler(this.rbTickets_Click);
            // 
            // rbSite
            // 
            this.rbSite.Image = global::Odebrecht_BlocoK.Properties.Resources.icons8_ajuda_40;
            this.rbSite.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbSite.SmallImage")));
            this.rbSite.Text = "Sobre";
            this.rbSite.Click += new System.EventHandler(this.rbSite_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 539);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(123, 17);
            this.toolStripStatusLabel1.Text = "http://odebrecht.com";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ribbon1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grupo Odebrecht - Escrituração Fiscal Digital (SPED/BlocoK)";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab rbDados;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonButton btnImportData;
        private System.Windows.Forms.RibbonButton btnMapas;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonButton ribbonButton4;
        private System.Windows.Forms.RibbonButton ribbonButton5;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel rbAjudaOnline;
        private System.Windows.Forms.RibbonButton rbTickets;
        private System.Windows.Forms.RibbonButton rbSite;
        private System.Windows.Forms.RibbonButton btnHistory;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonButton btnGerarBlocoK;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

