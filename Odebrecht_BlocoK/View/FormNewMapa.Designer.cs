﻿namespace Odebrecht_BlocoK.View
{
    partial class FormNewMapa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNewMapa));
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpOne = new System.Windows.Forms.TabPage();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRotuloCampo15 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo14 = new System.Windows.Forms.TextBox();
            this.txtCampo15 = new System.Windows.Forms.TextBox();
            this.txtCampo14 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo13 = new System.Windows.Forms.TextBox();
            this.txtCampo13 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo12 = new System.Windows.Forms.TextBox();
            this.txtCampo12 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo11 = new System.Windows.Forms.TextBox();
            this.txtCampo11 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo10 = new System.Windows.Forms.TextBox();
            this.txtCampo10 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo9 = new System.Windows.Forms.TextBox();
            this.txtCampo9 = new System.Windows.Forms.TextBox();
            this.txtRotuloCampo8 = new System.Windows.Forms.TextBox();
            this.txtCampo8 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo15 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo7 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo14 = new System.Windows.Forms.ComboBox();
            this.txtCampo7 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo13 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo6 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo12 = new System.Windows.Forms.ComboBox();
            this.txtCampo6 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo11 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo5 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo10 = new System.Windows.Forms.ComboBox();
            this.txtCampo5 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo9 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo4 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo8 = new System.Windows.Forms.ComboBox();
            this.txtCampo4 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo7 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo3 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo6 = new System.Windows.Forms.ComboBox();
            this.txtCampo3 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo5 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo2 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo4 = new System.Windows.Forms.ComboBox();
            this.txtCampo2 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo3 = new System.Windows.Forms.ComboBox();
            this.txtRotuloCampo1 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo2 = new System.Windows.Forms.ComboBox();
            this.txtCampo1 = new System.Windows.Forms.TextBox();
            this.cboTipoDadoCampo1 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnInsertMap = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtNomeMapa = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tbpOne.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 77);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 464);
            this.panel2.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbpOne);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 38);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(784, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // tbpOne
            // 
            this.tbpOne.Controls.Add(this.label47);
            this.tbpOne.Controls.Add(this.label44);
            this.tbpOne.Controls.Add(this.label41);
            this.tbpOne.Controls.Add(this.label38);
            this.tbpOne.Controls.Add(this.label35);
            this.tbpOne.Controls.Add(this.label32);
            this.tbpOne.Controls.Add(this.label29);
            this.tbpOne.Controls.Add(this.label26);
            this.tbpOne.Controls.Add(this.label23);
            this.tbpOne.Controls.Add(this.label20);
            this.tbpOne.Controls.Add(this.label17);
            this.tbpOne.Controls.Add(this.label14);
            this.tbpOne.Controls.Add(this.label11);
            this.tbpOne.Controls.Add(this.label8);
            this.tbpOne.Controls.Add(this.label5);
            this.tbpOne.Controls.Add(this.label46);
            this.tbpOne.Controls.Add(this.label43);
            this.tbpOne.Controls.Add(this.label40);
            this.tbpOne.Controls.Add(this.label37);
            this.tbpOne.Controls.Add(this.label34);
            this.tbpOne.Controls.Add(this.label31);
            this.tbpOne.Controls.Add(this.label28);
            this.tbpOne.Controls.Add(this.label25);
            this.tbpOne.Controls.Add(this.label22);
            this.tbpOne.Controls.Add(this.label19);
            this.tbpOne.Controls.Add(this.label16);
            this.tbpOne.Controls.Add(this.label13);
            this.tbpOne.Controls.Add(this.label10);
            this.tbpOne.Controls.Add(this.label7);
            this.tbpOne.Controls.Add(this.label4);
            this.tbpOne.Controls.Add(this.label45);
            this.tbpOne.Controls.Add(this.label42);
            this.tbpOne.Controls.Add(this.label39);
            this.tbpOne.Controls.Add(this.label36);
            this.tbpOne.Controls.Add(this.label33);
            this.tbpOne.Controls.Add(this.label30);
            this.tbpOne.Controls.Add(this.label27);
            this.tbpOne.Controls.Add(this.label24);
            this.tbpOne.Controls.Add(this.label21);
            this.tbpOne.Controls.Add(this.label18);
            this.tbpOne.Controls.Add(this.label15);
            this.tbpOne.Controls.Add(this.label12);
            this.tbpOne.Controls.Add(this.label9);
            this.tbpOne.Controls.Add(this.label6);
            this.tbpOne.Controls.Add(this.label3);
            this.tbpOne.Controls.Add(this.txtRotuloCampo15);
            this.tbpOne.Controls.Add(this.txtRotuloCampo14);
            this.tbpOne.Controls.Add(this.txtCampo15);
            this.tbpOne.Controls.Add(this.txtCampo14);
            this.tbpOne.Controls.Add(this.txtRotuloCampo13);
            this.tbpOne.Controls.Add(this.txtCampo13);
            this.tbpOne.Controls.Add(this.txtRotuloCampo12);
            this.tbpOne.Controls.Add(this.txtCampo12);
            this.tbpOne.Controls.Add(this.txtRotuloCampo11);
            this.tbpOne.Controls.Add(this.txtCampo11);
            this.tbpOne.Controls.Add(this.txtRotuloCampo10);
            this.tbpOne.Controls.Add(this.txtCampo10);
            this.tbpOne.Controls.Add(this.txtRotuloCampo9);
            this.tbpOne.Controls.Add(this.txtCampo9);
            this.tbpOne.Controls.Add(this.txtRotuloCampo8);
            this.tbpOne.Controls.Add(this.txtCampo8);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo15);
            this.tbpOne.Controls.Add(this.txtRotuloCampo7);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo14);
            this.tbpOne.Controls.Add(this.txtCampo7);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo13);
            this.tbpOne.Controls.Add(this.txtRotuloCampo6);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo12);
            this.tbpOne.Controls.Add(this.txtCampo6);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo11);
            this.tbpOne.Controls.Add(this.txtRotuloCampo5);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo10);
            this.tbpOne.Controls.Add(this.txtCampo5);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo9);
            this.tbpOne.Controls.Add(this.txtRotuloCampo4);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo8);
            this.tbpOne.Controls.Add(this.txtCampo4);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo7);
            this.tbpOne.Controls.Add(this.txtRotuloCampo3);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo6);
            this.tbpOne.Controls.Add(this.txtCampo3);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo5);
            this.tbpOne.Controls.Add(this.txtRotuloCampo2);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo4);
            this.tbpOne.Controls.Add(this.txtCampo2);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo3);
            this.tbpOne.Controls.Add(this.txtRotuloCampo1);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo2);
            this.tbpOne.Controls.Add(this.txtCampo1);
            this.tbpOne.Controls.Add(this.cboTipoDadoCampo1);
            this.tbpOne.Location = new System.Drawing.Point(4, 22);
            this.tbpOne.Name = "tbpOne";
            this.tbpOne.Padding = new System.Windows.Forms.Padding(3);
            this.tbpOne.Size = new System.Drawing.Size(776, 400);
            this.tbpOne.TabIndex = 0;
            this.tbpOne.Text = "Dados";
            this.tbpOne.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(22, 377);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(58, 13);
            this.label47.TabIndex = 2;
            this.label47.Text = "Campo 15:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(22, 351);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 13);
            this.label44.TabIndex = 2;
            this.label44.Text = "Campo 14:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(22, 325);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "Campo 13:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(22, 299);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(58, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Campo 12:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(22, 273);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Campo 11:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(22, 247);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(58, 13);
            this.label32.TabIndex = 2;
            this.label32.Text = "Campo 10:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(22, 221);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Campo 9:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(22, 195);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "Campo 8:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(22, 169);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Campo 7:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(22, 143);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Campo 6:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Campo 5:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 91);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Campo 4:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Campo 3:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Campo 2:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Campo 1:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(432, 377);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(75, 13);
            this.label46.TabIndex = 2;
            this.label46.Text = "Tipo de Dado:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(432, 351);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(75, 13);
            this.label43.TabIndex = 2;
            this.label43.Text = "Tipo de Dado:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(432, 325);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "Tipo de Dado:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(432, 299);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(75, 13);
            this.label37.TabIndex = 2;
            this.label37.Text = "Tipo de Dado:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(432, 273);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(75, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Tipo de Dado:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(432, 247);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(75, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Tipo de Dado:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(432, 221);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(75, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "Tipo de Dado:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(432, 195);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(75, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "Tipo de Dado:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(432, 169);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Tipo de Dado:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(432, 143);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Tipo de Dado:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(432, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Tipo de Dado:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(432, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Tipo de Dado:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(432, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Tipo de Dado:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(432, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Tipo de Dado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(432, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Tipo de Dado:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(186, 377);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 13);
            this.label45.TabIndex = 2;
            this.label45.Text = "Rótulo:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(186, 351);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 13);
            this.label42.TabIndex = 2;
            this.label42.Text = "Rótulo:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(186, 325);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 13);
            this.label39.TabIndex = 2;
            this.label39.Text = "Rótulo:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(186, 299);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 2;
            this.label36.Text = "Rótulo:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(186, 273);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 2;
            this.label33.Text = "Rótulo:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(186, 247);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 2;
            this.label30.Text = "Rótulo:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(186, 221);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Rótulo:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(186, 195);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Rótulo:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(186, 169);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 2;
            this.label21.Text = "Rótulo:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(186, 143);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Rótulo:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(186, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Rótulo:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(186, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Rótulo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(186, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Rótulo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(186, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Rótulo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(186, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rótulo:";
            // 
            // txtRotuloCampo15
            // 
            this.txtRotuloCampo15.Location = new System.Drawing.Point(233, 373);
            this.txtRotuloCampo15.Name = "txtRotuloCampo15";
            this.txtRotuloCampo15.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo15.TabIndex = 1;
            // 
            // txtRotuloCampo14
            // 
            this.txtRotuloCampo14.Location = new System.Drawing.Point(233, 347);
            this.txtRotuloCampo14.Name = "txtRotuloCampo14";
            this.txtRotuloCampo14.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo14.TabIndex = 1;
            // 
            // txtCampo15
            // 
            this.txtCampo15.Location = new System.Drawing.Point(81, 373);
            this.txtCampo15.Name = "txtCampo15";
            this.txtCampo15.Size = new System.Drawing.Size(99, 20);
            this.txtCampo15.TabIndex = 1;
            // 
            // txtCampo14
            // 
            this.txtCampo14.Location = new System.Drawing.Point(81, 347);
            this.txtCampo14.Name = "txtCampo14";
            this.txtCampo14.Size = new System.Drawing.Size(99, 20);
            this.txtCampo14.TabIndex = 1;
            // 
            // txtRotuloCampo13
            // 
            this.txtRotuloCampo13.Location = new System.Drawing.Point(233, 321);
            this.txtRotuloCampo13.Name = "txtRotuloCampo13";
            this.txtRotuloCampo13.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo13.TabIndex = 1;
            // 
            // txtCampo13
            // 
            this.txtCampo13.Location = new System.Drawing.Point(81, 321);
            this.txtCampo13.Name = "txtCampo13";
            this.txtCampo13.Size = new System.Drawing.Size(99, 20);
            this.txtCampo13.TabIndex = 1;
            // 
            // txtRotuloCampo12
            // 
            this.txtRotuloCampo12.Location = new System.Drawing.Point(233, 295);
            this.txtRotuloCampo12.Name = "txtRotuloCampo12";
            this.txtRotuloCampo12.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo12.TabIndex = 1;
            // 
            // txtCampo12
            // 
            this.txtCampo12.Location = new System.Drawing.Point(81, 295);
            this.txtCampo12.Name = "txtCampo12";
            this.txtCampo12.Size = new System.Drawing.Size(99, 20);
            this.txtCampo12.TabIndex = 1;
            // 
            // txtRotuloCampo11
            // 
            this.txtRotuloCampo11.Location = new System.Drawing.Point(233, 269);
            this.txtRotuloCampo11.Name = "txtRotuloCampo11";
            this.txtRotuloCampo11.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo11.TabIndex = 1;
            // 
            // txtCampo11
            // 
            this.txtCampo11.Location = new System.Drawing.Point(81, 269);
            this.txtCampo11.Name = "txtCampo11";
            this.txtCampo11.Size = new System.Drawing.Size(99, 20);
            this.txtCampo11.TabIndex = 1;
            // 
            // txtRotuloCampo10
            // 
            this.txtRotuloCampo10.Location = new System.Drawing.Point(233, 243);
            this.txtRotuloCampo10.Name = "txtRotuloCampo10";
            this.txtRotuloCampo10.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo10.TabIndex = 1;
            // 
            // txtCampo10
            // 
            this.txtCampo10.Location = new System.Drawing.Point(81, 243);
            this.txtCampo10.Name = "txtCampo10";
            this.txtCampo10.Size = new System.Drawing.Size(99, 20);
            this.txtCampo10.TabIndex = 1;
            // 
            // txtRotuloCampo9
            // 
            this.txtRotuloCampo9.Location = new System.Drawing.Point(233, 217);
            this.txtRotuloCampo9.Name = "txtRotuloCampo9";
            this.txtRotuloCampo9.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo9.TabIndex = 1;
            // 
            // txtCampo9
            // 
            this.txtCampo9.Location = new System.Drawing.Point(81, 217);
            this.txtCampo9.Name = "txtCampo9";
            this.txtCampo9.Size = new System.Drawing.Size(99, 20);
            this.txtCampo9.TabIndex = 1;
            // 
            // txtRotuloCampo8
            // 
            this.txtRotuloCampo8.Location = new System.Drawing.Point(233, 191);
            this.txtRotuloCampo8.Name = "txtRotuloCampo8";
            this.txtRotuloCampo8.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo8.TabIndex = 1;
            // 
            // txtCampo8
            // 
            this.txtCampo8.Location = new System.Drawing.Point(81, 191);
            this.txtCampo8.Name = "txtCampo8";
            this.txtCampo8.Size = new System.Drawing.Size(99, 20);
            this.txtCampo8.TabIndex = 1;
            // 
            // cboTipoDadoCampo15
            // 
            this.cboTipoDadoCampo15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo15.FormattingEnabled = true;
            this.cboTipoDadoCampo15.Location = new System.Drawing.Point(513, 373);
            this.cboTipoDadoCampo15.Name = "cboTipoDadoCampo15";
            this.cboTipoDadoCampo15.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo15.TabIndex = 0;
            // 
            // txtRotuloCampo7
            // 
            this.txtRotuloCampo7.Location = new System.Drawing.Point(233, 165);
            this.txtRotuloCampo7.Name = "txtRotuloCampo7";
            this.txtRotuloCampo7.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo7.TabIndex = 1;
            // 
            // cboTipoDadoCampo14
            // 
            this.cboTipoDadoCampo14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo14.FormattingEnabled = true;
            this.cboTipoDadoCampo14.Location = new System.Drawing.Point(513, 347);
            this.cboTipoDadoCampo14.Name = "cboTipoDadoCampo14";
            this.cboTipoDadoCampo14.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo14.TabIndex = 0;
            // 
            // txtCampo7
            // 
            this.txtCampo7.Location = new System.Drawing.Point(81, 165);
            this.txtCampo7.Name = "txtCampo7";
            this.txtCampo7.Size = new System.Drawing.Size(99, 20);
            this.txtCampo7.TabIndex = 1;
            // 
            // cboTipoDadoCampo13
            // 
            this.cboTipoDadoCampo13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo13.FormattingEnabled = true;
            this.cboTipoDadoCampo13.Location = new System.Drawing.Point(513, 321);
            this.cboTipoDadoCampo13.Name = "cboTipoDadoCampo13";
            this.cboTipoDadoCampo13.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo13.TabIndex = 0;
            // 
            // txtRotuloCampo6
            // 
            this.txtRotuloCampo6.Location = new System.Drawing.Point(233, 139);
            this.txtRotuloCampo6.Name = "txtRotuloCampo6";
            this.txtRotuloCampo6.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo6.TabIndex = 1;
            // 
            // cboTipoDadoCampo12
            // 
            this.cboTipoDadoCampo12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo12.FormattingEnabled = true;
            this.cboTipoDadoCampo12.Location = new System.Drawing.Point(513, 295);
            this.cboTipoDadoCampo12.Name = "cboTipoDadoCampo12";
            this.cboTipoDadoCampo12.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo12.TabIndex = 0;
            // 
            // txtCampo6
            // 
            this.txtCampo6.Location = new System.Drawing.Point(81, 139);
            this.txtCampo6.Name = "txtCampo6";
            this.txtCampo6.Size = new System.Drawing.Size(99, 20);
            this.txtCampo6.TabIndex = 1;
            // 
            // cboTipoDadoCampo11
            // 
            this.cboTipoDadoCampo11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo11.FormattingEnabled = true;
            this.cboTipoDadoCampo11.Location = new System.Drawing.Point(513, 269);
            this.cboTipoDadoCampo11.Name = "cboTipoDadoCampo11";
            this.cboTipoDadoCampo11.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo11.TabIndex = 0;
            // 
            // txtRotuloCampo5
            // 
            this.txtRotuloCampo5.Location = new System.Drawing.Point(233, 113);
            this.txtRotuloCampo5.Name = "txtRotuloCampo5";
            this.txtRotuloCampo5.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo5.TabIndex = 1;
            // 
            // cboTipoDadoCampo10
            // 
            this.cboTipoDadoCampo10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo10.FormattingEnabled = true;
            this.cboTipoDadoCampo10.Location = new System.Drawing.Point(513, 243);
            this.cboTipoDadoCampo10.Name = "cboTipoDadoCampo10";
            this.cboTipoDadoCampo10.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo10.TabIndex = 0;
            // 
            // txtCampo5
            // 
            this.txtCampo5.Location = new System.Drawing.Point(81, 113);
            this.txtCampo5.Name = "txtCampo5";
            this.txtCampo5.Size = new System.Drawing.Size(99, 20);
            this.txtCampo5.TabIndex = 1;
            // 
            // cboTipoDadoCampo9
            // 
            this.cboTipoDadoCampo9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo9.FormattingEnabled = true;
            this.cboTipoDadoCampo9.Location = new System.Drawing.Point(513, 217);
            this.cboTipoDadoCampo9.Name = "cboTipoDadoCampo9";
            this.cboTipoDadoCampo9.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo9.TabIndex = 0;
            // 
            // txtRotuloCampo4
            // 
            this.txtRotuloCampo4.Location = new System.Drawing.Point(233, 87);
            this.txtRotuloCampo4.Name = "txtRotuloCampo4";
            this.txtRotuloCampo4.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo4.TabIndex = 1;
            // 
            // cboTipoDadoCampo8
            // 
            this.cboTipoDadoCampo8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo8.FormattingEnabled = true;
            this.cboTipoDadoCampo8.Location = new System.Drawing.Point(513, 191);
            this.cboTipoDadoCampo8.Name = "cboTipoDadoCampo8";
            this.cboTipoDadoCampo8.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo8.TabIndex = 0;
            // 
            // txtCampo4
            // 
            this.txtCampo4.Location = new System.Drawing.Point(81, 87);
            this.txtCampo4.Name = "txtCampo4";
            this.txtCampo4.Size = new System.Drawing.Size(99, 20);
            this.txtCampo4.TabIndex = 1;
            // 
            // cboTipoDadoCampo7
            // 
            this.cboTipoDadoCampo7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo7.FormattingEnabled = true;
            this.cboTipoDadoCampo7.Location = new System.Drawing.Point(513, 165);
            this.cboTipoDadoCampo7.Name = "cboTipoDadoCampo7";
            this.cboTipoDadoCampo7.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo7.TabIndex = 0;
            // 
            // txtRotuloCampo3
            // 
            this.txtRotuloCampo3.Location = new System.Drawing.Point(233, 61);
            this.txtRotuloCampo3.Name = "txtRotuloCampo3";
            this.txtRotuloCampo3.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo3.TabIndex = 1;
            // 
            // cboTipoDadoCampo6
            // 
            this.cboTipoDadoCampo6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo6.FormattingEnabled = true;
            this.cboTipoDadoCampo6.Location = new System.Drawing.Point(513, 139);
            this.cboTipoDadoCampo6.Name = "cboTipoDadoCampo6";
            this.cboTipoDadoCampo6.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo6.TabIndex = 0;
            // 
            // txtCampo3
            // 
            this.txtCampo3.Location = new System.Drawing.Point(81, 61);
            this.txtCampo3.Name = "txtCampo3";
            this.txtCampo3.Size = new System.Drawing.Size(99, 20);
            this.txtCampo3.TabIndex = 1;
            // 
            // cboTipoDadoCampo5
            // 
            this.cboTipoDadoCampo5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo5.FormattingEnabled = true;
            this.cboTipoDadoCampo5.Location = new System.Drawing.Point(513, 113);
            this.cboTipoDadoCampo5.Name = "cboTipoDadoCampo5";
            this.cboTipoDadoCampo5.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo5.TabIndex = 0;
            // 
            // txtRotuloCampo2
            // 
            this.txtRotuloCampo2.Location = new System.Drawing.Point(233, 35);
            this.txtRotuloCampo2.Name = "txtRotuloCampo2";
            this.txtRotuloCampo2.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo2.TabIndex = 1;
            // 
            // cboTipoDadoCampo4
            // 
            this.cboTipoDadoCampo4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo4.FormattingEnabled = true;
            this.cboTipoDadoCampo4.Location = new System.Drawing.Point(513, 87);
            this.cboTipoDadoCampo4.Name = "cboTipoDadoCampo4";
            this.cboTipoDadoCampo4.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo4.TabIndex = 0;
            // 
            // txtCampo2
            // 
            this.txtCampo2.Location = new System.Drawing.Point(81, 35);
            this.txtCampo2.Name = "txtCampo2";
            this.txtCampo2.Size = new System.Drawing.Size(99, 20);
            this.txtCampo2.TabIndex = 1;
            // 
            // cboTipoDadoCampo3
            // 
            this.cboTipoDadoCampo3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo3.FormattingEnabled = true;
            this.cboTipoDadoCampo3.Location = new System.Drawing.Point(513, 61);
            this.cboTipoDadoCampo3.Name = "cboTipoDadoCampo3";
            this.cboTipoDadoCampo3.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo3.TabIndex = 0;
            // 
            // txtRotuloCampo1
            // 
            this.txtRotuloCampo1.Location = new System.Drawing.Point(233, 9);
            this.txtRotuloCampo1.Name = "txtRotuloCampo1";
            this.txtRotuloCampo1.Size = new System.Drawing.Size(184, 20);
            this.txtRotuloCampo1.TabIndex = 1;
            // 
            // cboTipoDadoCampo2
            // 
            this.cboTipoDadoCampo2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo2.FormattingEnabled = true;
            this.cboTipoDadoCampo2.Location = new System.Drawing.Point(513, 35);
            this.cboTipoDadoCampo2.Name = "cboTipoDadoCampo2";
            this.cboTipoDadoCampo2.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo2.TabIndex = 0;
            // 
            // txtCampo1
            // 
            this.txtCampo1.Location = new System.Drawing.Point(81, 9);
            this.txtCampo1.Name = "txtCampo1";
            this.txtCampo1.Size = new System.Drawing.Size(99, 20);
            this.txtCampo1.TabIndex = 1;
            // 
            // cboTipoDadoCampo1
            // 
            this.cboTipoDadoCampo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDadoCampo1.FormattingEnabled = true;
            this.cboTipoDadoCampo1.Location = new System.Drawing.Point(513, 9);
            this.cboTipoDadoCampo1.Name = "cboTipoDadoCampo1";
            this.cboTipoDadoCampo1.Size = new System.Drawing.Size(154, 21);
            this.cboTipoDadoCampo1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 77);
            this.panel1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Crie ou edite um mapa de importação de dados do 3DW";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Criar/Editar Mapa de Importação";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnInsertMap);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 541);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(784, 36);
            this.panel3.TabIndex = 5;
            // 
            // btnInsertMap
            // 
            this.btnInsertMap.Location = new System.Drawing.Point(568, 7);
            this.btnInsertMap.Name = "btnInsertMap";
            this.btnInsertMap.Size = new System.Drawing.Size(103, 23);
            this.btnInsertMap.TabIndex = 0;
            this.btnInsertMap.Text = "Salvar";
            this.btnInsertMap.UseVisualStyleBackColor = true;
            this.btnInsertMap.Click += new System.EventHandler(this.btnInsertMap_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtNomeMapa);
            this.panel4.Controls.Add(this.label48);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(784, 38);
            this.panel4.TabIndex = 1;
            // 
            // txtNomeMapa
            // 
            this.txtNomeMapa.Location = new System.Drawing.Point(70, 9);
            this.txtNomeMapa.Name = "txtNomeMapa";
            this.txtNomeMapa.Size = new System.Drawing.Size(601, 20);
            this.txtNomeMapa.TabIndex = 0;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 12);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(38, 13);
            this.label48.TabIndex = 2;
            this.label48.Text = "Nome:";
            // 
            // FormNewMapa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 577);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormNewMapa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "3DW Lite - Novo Mapa";
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tbpOne.ResumeLayout(false);
            this.tbpOne.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbpOne;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRotuloCampo14;
        private System.Windows.Forms.TextBox txtCampo14;
        private System.Windows.Forms.TextBox txtRotuloCampo13;
        private System.Windows.Forms.TextBox txtCampo13;
        private System.Windows.Forms.TextBox txtRotuloCampo12;
        private System.Windows.Forms.TextBox txtCampo12;
        private System.Windows.Forms.TextBox txtRotuloCampo11;
        private System.Windows.Forms.TextBox txtCampo11;
        private System.Windows.Forms.TextBox txtRotuloCampo10;
        private System.Windows.Forms.TextBox txtCampo10;
        private System.Windows.Forms.TextBox txtRotuloCampo9;
        private System.Windows.Forms.TextBox txtCampo9;
        private System.Windows.Forms.TextBox txtRotuloCampo8;
        private System.Windows.Forms.TextBox txtCampo8;
        private System.Windows.Forms.TextBox txtRotuloCampo7;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo14;
        private System.Windows.Forms.TextBox txtCampo7;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo13;
        private System.Windows.Forms.TextBox txtRotuloCampo6;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo12;
        private System.Windows.Forms.TextBox txtCampo6;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo11;
        private System.Windows.Forms.TextBox txtRotuloCampo5;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo10;
        private System.Windows.Forms.TextBox txtCampo5;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo9;
        private System.Windows.Forms.TextBox txtRotuloCampo4;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo8;
        private System.Windows.Forms.TextBox txtCampo4;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo7;
        private System.Windows.Forms.TextBox txtRotuloCampo3;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo6;
        private System.Windows.Forms.TextBox txtCampo3;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo5;
        private System.Windows.Forms.TextBox txtRotuloCampo2;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo4;
        private System.Windows.Forms.TextBox txtCampo2;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo3;
        private System.Windows.Forms.TextBox txtRotuloCampo1;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo2;
        private System.Windows.Forms.TextBox txtCampo1;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtRotuloCampo15;
        private System.Windows.Forms.TextBox txtCampo15;
        private System.Windows.Forms.ComboBox cboTipoDadoCampo15;
        private System.Windows.Forms.Button btnInsertMap;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtNomeMapa;
        private System.Windows.Forms.Label label48;
    }
}