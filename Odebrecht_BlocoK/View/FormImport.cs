﻿using _3DW.Core;
using Odebrecht_BlocoK.Adapters;
using Odebrecht_BlocoK.Model;
using Odebrecht_BlocoK.Model.Settings;
using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK.View
{
    public partial class FormImport : Form
    {
        private Root Settings { get; set; }
        private String CARIMBO { get; set; }
        private AdapterResponse mapResponse { get; set; }
        private String FileName
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        public DataTable dtContent { get; set; }

        public FormImport(Root Settings)
        {
            InitializeComponent();
            this.Settings = Settings;

            cboMes.SelectedIndex = cboMes.FindStringExact(DateTime.Now.Month.ToString());
            cboAno.Items.Add(DateTime.Now.Year.ToString());
            cboAno.SelectedIndex = 0;
            LoadCarimbos();
            LoadMaps();
            BindData();
        }

        private void LoadCarimbos()
        {
            String CompleteHash = MD5Utils.CalculateMD5Hash(String.Concat(DateTime.Now.ToLongDateString()));
            String UniqueID = CompleteHash.Substring(CompleteHash.Length - 9);

            this.CARIMBO = String.Format("CAODB{0}{1}{2}{3}{4}{5}{6}_{7}",
                DateTime.Now.Day,
                DateTime.Now.Month,
                DateTime.Now.Year,
                DateTime.Now.Hour,
                DateTime.Now.Minute,
                DateTime.Now.Second,
                DateTime.Now.Millisecond,
                UniqueID);

            this.txtCarimbo.Text = this.CARIMBO;
        }

        private void LoadMaps()
        {
            Model.Settings.DataSource ResourceMain = Settings.DataSources[Settings.DefaultDataSourceIndex];
            String SQL = String.Format("SELECT * FROM maps");

            this.mapResponse = AdapterResponse.FromJSON(Caller.Invoke(ResourceMain.Driver, "GetData", new Object[] {
                    ResourceMain.Embeded ? CommonUtils.ToCurrentPath(ResourceMain.FilePath) :  ResourceMain.FilePath,
                    SQL
                }));

            if (mapResponse.Success)
            {
                cboMapa.DataSource = mapResponse.Data;
                cboMapa.DisplayMember = "name";
            }
        }

        private void BindData()
        {
            cboDataSource.DataSource = this.Settings.ImportDataFiles;
            cboDataSource.DisplayMember = "Name";
            cboDataSource.ValueMember= "Extensions";
        }

        private void chkGerarMapaAutomaticamente_CheckedChanged(object sender, EventArgs e)
        {
            //cboMapa.Enabled = !chkGerarMapaAutomaticamente.Checked;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            ImportDataFile importData = Settings.ImportDataFiles[cboDataSource.SelectedIndex];
            String FilePath = Browser.OpenFile(importData.Extensions, importData.Name);

            if (!FilePath.Equals(String.Empty))
            {
                textBox1.Text = FilePath;
                BindSheetNames(FilePath);
                btnImport.Enabled = false;
            }
        }

        private void BindSheetNames(String Path)
        {
            try
            {
                String[] Names = _3DW.Core.Adapters.Excel.GetExcelSheetNames(Path);
                cboTables.Items.AddRange(Names);

                if (Names.Length > 0)
                {
                    cboTables.SelectedIndex = 0;
                }
            }catch(Exception ex)
            {
                Log.Make(ex);
            }
        }

        private void btnLoadDataSet_Click(object sender, EventArgs e)
        {
            LoadDataSet();
        }

        private void LoadDataSet()
        {
            try
            {
                DataSet source = _3DW.Core.Adapters.Excel.ExcelToDataset(FileName);
                dtgDados.AutoGenerateColumns = true;

                dtContent = source.Tables[cboTables.SelectedIndex].Copy();

                if (chkGerarMapaAutomaticamente.Checked)
                {
                    dtContent.Rows[0].Delete();
                    dtgDados.DataSource = dtContent;

                    for(int Index = 0; Index < dtgDados.Columns.Count; Index++)
                    {
                        String Title = source.Tables[cboTables.SelectedIndex].Rows[0][Index].ToString();
                        dtgDados.Columns[Index].HeaderText = Title.Equals(String.Empty) ? String.Format("Coluna{0}", Index+1) : Title;
                        Index++;
                    }
                }
                else
                {
                    dtgDados.DataSource = dtContent;
                }
                
                btnImport.Enabled = true;
            }
            catch (Exception ex)
            {
                Log.Make(ex);
            }
        }

        private void cboTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadDataSet();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Settings.SkipSelectDataSourceWindow)
            {
                Model.Settings.DataSource ResourceMain = Settings.DataSources[Settings.DefaultDataSourceIndex];

                AdapterResponse response = AdapterResponse.FromJSON(Caller.Invoke(ResourceMain.Driver, "BulkInsert", new Object[] {
                    dtContent,
                    ResourceMain.Embeded ? CommonUtils.ToCurrentPath(ResourceMain.FilePath) :  ResourceMain.FilePath,
                    prbLoading,
                    cboMes.Text,
                    cboAno.Text,
                    mapResponse.Data.Rows[cboMapa.SelectedIndex]["id"].ToString(),
                    CARIMBO
                }));

                response.ShowDialod();

                btnGerarBloco.Enabled = response.Success;
            }

        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            LoadMaps();
        }

        private void btnNewMapa_Click(object sender, EventArgs e)
        {
            FormNewMapa mapa = new FormNewMapa(this.Settings);
            mapa.ShowDialog();
        }

        private void btnGerarBloco_Click(object sender, EventArgs e)
        {
            FormBlocoK blocoK = new FormBlocoK(this.Settings, cboMes.Text, cboAno.Text, txtCarimbo.Text, this);
            blocoK.ShowDialog();
        }
    }
}
