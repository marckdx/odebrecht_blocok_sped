--
-- File generated with SQLiteStudio v3.2.1 on seg ago 13 22:42:19 2018
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: imports
CREATE TABLE imports (id INTEGER PRIMARY KEY, campo1 VARCHAR (255), campo2 VARCHAR (255), campo3 VARCHAR (255), campo4 VARCHAR, campo5 VARCHAR (255), campo6 VARCHAR (255), campo7 VARCHAR (255), campo8 VARCHAR (255), campo9 VARCHAR (255), campo10 VARCHAR (255), campo11 VARCHAR (255), campo12 VARCHAR (255), campo13 VARCHAR (255), campo14 VARCHAR (255), campo15 VARCHAR (255), campo16 VARCHAR (255), campo17 VARCHAR (255), campo18 VARCHAR (255), campo19 VARCHAR (255), campo20 VARCHAR (255), mes INTEGER (12), ano INTEGER (12), criado_em VARCHAR (255), criado_por VARCHAR (255));

-- Table: maps
CREATE TABLE maps (id INTEGER PRIMARY KEY, name VARCHAR (255), rotulo_campo1 VARCHAR (255), rotulo_campo2 VARCHAR (255), rotulo_campo3 VARCHAR (255), rotulo_campo4 VARCHAR (255), rotulo_campo5 VARCHAR (255), rotulo_campo6 VARCHAR (255), rotulo_campo21 VARCHAR (255), rotulo_campo7 VARCHAR (255), rotulo_campo8 VARCHAR (255), rotulo_campo9 VARCHAR (255), rotulo_campo10 VARCHAR (255), rotulo_campo11 VARCHAR (255), rotulo_campo12 VARCHAR (255), rotulo_campo13 VARCHAR (255), rotulo_campo14 VARCHAR (255), rotulo_campo15 VARCHAR (255), rotulo_campo16 VARCHAR (255), rotulo_campo17 VARCHAR (255), rotulo_campo18 VARCHAR (255), rotulo_campo19 VARCHAR (255), rotulo_campo20 VARCHAR (255));

-- Table: views
CREATE TABLE views (id INTEGER PRIMARY KEY);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
