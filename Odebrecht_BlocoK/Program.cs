﻿using Odebrecht_BlocoK.Model;
using Odebrecht_BlocoK.Model.Settings;
using Odebrecht_BlocoK.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Odebrecht_BlocoK
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            // Lê o arquivo de configurações
            Root root = Root.FromFile(CommonUtils.ToCurrentPath(@"\Data\Settings.json"));
            Application.Run(new FormMain(root));
        }
    }
}
