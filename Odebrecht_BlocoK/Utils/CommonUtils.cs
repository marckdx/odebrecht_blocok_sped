﻿using Odebrecht_BlocoK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Odebrecht_BlocoK.Utils
{
    public class CommonUtils
    {
        public static String CURRENT_PATH
        {
            get
            {
                return System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
        }

        public static String ToCurrentPath(String Semi)
        {
            return String.Concat(CURRENT_PATH, @"\", Semi);
        }

        public static string MakeOnlyFour(string content)
        {
            Int32 length = content.Length;
            if (length <= 4)
            {
                return Plus(content, 4);
            }
            else if (length >= 5)
            {
                return content.Remove(0, length - 4);
            }

            return String.Empty;
        }

        private static string Plus(String Origin, Int32 Remove, Boolean doSomething = false)
        {
            if (doSomething)
            {
                String Append = String.Empty;
                for (int i = 1; i < Remove; i++)
                {
                    Append += "0";
                }

                return String.Concat(Append, Origin);
            }
            else
            {
                return Origin;
            }
        }

        public static Double ToDouble(String VALOR_STR)
        {
            try
            {
                //VALOR_STR = VALOR_STR.Replace(".", String.Empty);
                //VALOR_STR = VALOR_STR.Replace(",", ".");
                return Convert.ToDouble(VALOR_STR);
            }catch(Exception ex)
            {
                Log.Make(ex);
                return 0d;
            }
        }
    }
}
