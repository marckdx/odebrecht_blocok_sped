﻿using ExcelDataReader;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3DW.Core.Adapters
{
    public class Excel
    {
        public static DataSet ExcelToDataset(String WorkbookPath)
        {
            FileStream stream = File.Open(WorkbookPath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            if (WorkbookPath.EndsWith(".xls"))
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (WorkbookPath.EndsWith(".xlsx"))
            {
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }
            //excelReader.IsFirstRowAsColumnNames = true;
            DataSet result = excelReader.AsDataSet();
           
            excelReader.Close();

            return result;
        }


        public static string[] GetExcelSheetNames(string connectionString)
        {
            List<String> Sheets = new List<String>();
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
            excelApp.DisplayAlerts = false;
            excelApp.Visible = false;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;

            try
            {
                excelWorkbook = excelApp.Workbooks.Open(connectionString, 0,
                false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true,
                false, 0, true, false, false);
            }
            catch
            {
                excelWorkbook = excelApp.Workbooks.Add();
            }

            Microsoft.Office.Interop.Excel.Sheets excelSheets = excelWorkbook.Worksheets;

            foreach(Worksheet worksheet in excelSheets)
            {
                Sheets.Add(worksheet.Name);
            }

            excelWorkbook.Close();

            return Sheets.ToArray();
        }
    }
}
