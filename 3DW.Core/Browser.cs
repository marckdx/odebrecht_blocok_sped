﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3DW.Core
{
    public class Browser
    {
        public static String OpenFile(String Extensions, String FileType)
        {
            return OpenFile(Extensions.Split(';'), FileType);
        }

        public static String OpenFile(String[] Extensions, String FileType)
        {
            String Filter = String.Empty;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            foreach(String Extension in Extensions)
            {
                Filter += String.Format("Arquivos {0}|*{1}|", FileType, Extension);
            }
            Filter = Filter.Remove(Filter.Length - 1);

            openFileDialog1.Title = String.Format("Selecione um arquivo {0}", FileType);
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }

            return String.Empty;
        }
    }
}
